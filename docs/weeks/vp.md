**WEEK 2**

**Video Production**

We learned how to install Adobe Premiere

**Steps of Video editing**

- Open Adobe Premiere after installing

![tag](../img/open.png)

- Create New project 

![tag](../img/new.png)

- Give a name 
- Capture Format select HDV

![tag](../img/name.png)

- Import File

![tag](../img/import.png)

- Workspace
- Drag video to the timeline right

![tag](../img/work.png)

- Select effects

![tag](../img/effect.png)

- Search for 'Ultra key" 
- Drag to right on video
- Effect control

![tag](../img/ultra.png)

- Change Key color and remove background

![tag](../img/key.png)

- Import (Ctrl+I) a background picture
- Drag to right between video and audio
- If to small, make it biger with "scale"

![tag](../img/backkground.png)

- Add Title 

![tag](../img/title.png)

- Give it a name

![tag](../img/title2.png)

- Type title 
- Select any Text Font
- Select any color

![tag](../img/color.png)

- Drag Title to right between video and background
- Stretch as pleased

![tag](../img/drag.png)

- Effects
- Video Transitions
- Select any and drag to timeline right

![tag](../img/video.png)

- Export file

![tag](../img/export.png)

- Export settings
- Format H.264

![tag](../img/settingss.png)

- Select 'preset' 
- Youtube 720p HD

![tag](../img/youtube.png)

- Output Name
 
![tag](../img/outputt.png)

- Save as 
 
![tag](../img/save.png)

- Click Export

![tag](../img/final.png)
