**SCLERAYA**

Nowadays, Security system is one of the most researched fields and with increasing security threats, companies are launching new smart security products to combat these threats. **IoT** is an added advantage in this field which can automatically trigger an event.

I made a Smart Wi-Fi Video Doorbell using ESP32-CAM, named **SCLERAYA**

A ring at the doorbell may inflame a sense of expectation, suspense, secrecy, hazard or even intrigue.
This Smart doorbell can easily be powered by a **3.7V Battery** and whenever someone at the door **presses** the doorbell button, it will play **a specific song** on your phone and sends a **text message** with a **link** of video streaming page where you can see the person at the door/gate.

**FIRST** I build a Face Recognition System using ESP32-CAM which will also work as an ESP32-CAM Security system by recognizing the face of unauthorized persons. ESP32-CAM is a very small camera module with the ESP32-S chip. Using the ESP32-CAM module we can build a face recognition system. 

**INTRODUCTION TO ESP32-CAM**

The **AI-Thinker ESP32-CAM** module comes with an ESP32-S chip, a very small size OV2640 camera and a micro SD card slot. Micro SD card slot can be used to store images taken from the camera or to store files. This ESP32-CAM module can be widely used in various IoT applications.

![tag](../img/aal1.png) 

The **ESP32-CAM** module can be programmed with **Arduino IDE**. 

**ESP32-CAM** module also has several **GPIO pins** to connect the external hardware. The ESP32-CAM doesn’t have a USB connector, so to program the module you need an **FTDI board** (I used FTDI USB CABLE).

1 |  2
--- | ---
![tag](../img/aal2.png)  | ![tag](../img/aal10.jpg)


You’ll also note a square **white LED** on the top of the module, this can act as a “flash” for illuminating the subject you are trying to view with the camera.

**ESP32-CAM SPECIFICATIONS**
- 802.11b/g/n Wi-Fi
- Bluetooth 4.2 with BLE
- UART, SPI, I2C and PWM interfaces
- Clock speed up to 160 MHz
- Computing power up to 600 DMIPS
- 520 KB SRAM plus 4 MB PSRAM
- Supports WiFi Image Upload
- Multiple Sleep modes
- Firmware Over the Air (FOTA) upgrades possible
- 9 GPIO ports
- Built-in Flash LED

**CAMERA SPECIFICATIONS**
- 2 Megapixel sensor
- Array size UXGA 1622×1200
- Output formats include YUV422, YUV420, RGB565, RGB555 and 8-bit compressed data
- Image transfer rate of 15 to 60 fps

**COMPONENTS REQUIRED**
- ESP32-CAM
- FTDI Programmer

**CIRCUIT DIAGRAM**

![tag](../img/aal10.png)

**Vcc** and **GND** pin of ESP32 is connected with the **Vcc** and **GND** pin of the FTDI board. **Tx** of and **Rx** of ESP32 is connected with **Rx** and **Tx** of the FTDI board.

| ESP32-Cam | FTDI |
| ------ | ------ |
| 3.3V | VCC |
| GND | GND |
| UOR | TX  |
| UOT | RX  |

**Note**: Before uploading the code connect the IO0 to ground. IO0 determines whether the ESP32 is in flashing mode or not. When GPIO 0 is connected to GND, the ESP32 is in flashing mode.

**INSTALL ESP32 BOARD ON ARDUINO IDE**

---
- To install the ESP32 board in your Arduino IDE, go to **File> Preferences**


![tag](../img/aal3.png)

---
- Now copy the below link and paste it into the **“Additional Board Manager URLs”** field as shown in the picture below. Then, click the **“OK”** button:


[https://dl.espressif.com/dl/package_esp32_index.json]()

![tag](../img/aal12.png)

---
- Now go to **Tools > Board > Boards Manager**

![tag](../img/aal13.png)

---
- In Board Manager search for ESP32 and install the **“ESP32 by Espressif Systems“**.

![tag](../img/aal14.png)

**ESP32 CAMERA WEBSERVER CODE**
---
We already have an example code from ESP32-cam video streaming and face recognition. Open the ESP32 example by using **File > Examples > ESP32 > Camera** and open the **CameraWebServer** example.

![tag](../img/aal15.png)

- Before uploading the code, you need to enter your Wi-Fi **name** and **password**.

```ruby
const char* ssid = "WiFi Name";
const char* password = "Password";

```

**NOW THE CODE IS READY TO UPLOAD**

To upload the code, connect the FDTI to your laptop and select the ‘Al thinker ESP32-CAM’ as your board.

![tag](../img/aal16.png)

After that define the ESP camera module. In the code, they have defined 5 camera modules so uncomment the **“CAMERA_MODEL_AI_THINKER”** and comment rest of the modules.

After uploading the code disconnect the IO0 and GND pin. Then open the serial monitor and change the baud rate to 115200. After that press the ESP32 reset button it will print the ESP IP address and port no on a serial monitor.

---
- Now to access the camera streaming, navigate to your browser and enter your ESP IP address. It will take you to the streaming page. To start the ESP32 cam video streaming click on ‘Start Stream’ button at the bottom of the page.
- After testing the video streaming, test the ESP32-cam face detection and recognition features. For that turn on the Face recognition and detection features from settings.


1 |  2
--- | ---
![tag](../img/aal7.png) | ![tag](../img/aal8.png)

---

 
**THE CODE**

```ruby

#include "esp_camera.h"

#include <WiFi.h>

// WARNING!!! Make sure that you have either selected ESP32 Wrover Module,

//  or another board which has PSRAM enabled

// Select camera model

//#define CAMERA_MODEL_WROVER_KIT

//#define CAMERA_MODEL_ESP_EYE

//#define CAMERA_MODEL_M5STACK_PSRAM

//#define CAMERA_MODEL_M5STACK_WIDE

#define CAMERA_MODEL_AI_THINKER

#include "camera_pins.h"

const char* ssid = "ssid";

const char* password = "password";

void startCameraServer();

void setup() {

  Serial.begin(115200);

  Serial.setDebugOutput(true);

  Serial.println();

  camera_config_t config;

  config.ledc_channel = LEDC_CHANNEL_0;

  config.ledc_timer = LEDC_TIMER_0;

  config.pin_d0 = Y2_GPIO_NUM;

  config.pin_d1 = Y3_GPIO_NUM;

  config.pin_d2 = Y4_GPIO_NUM;

  config.pin_d3 = Y5_GPIO_NUM;

  config.pin_d4 = Y6_GPIO_NUM;

  config.pin_d5 = Y7_GPIO_NUM;

  config.pin_d6 = Y8_GPIO_NUM;

  config.pin_d7 = Y9_GPIO_NUM;

  config.pin_xclk = XCLK_GPIO_NUM;

  config.pin_pclk = PCLK_GPIO_NUM;

  config.pin_vsync = VSYNC_GPIO_NUM;

  config.pin_href = HREF_GPIO_NUM;

  config.pin_sscb_sda = SIOD_GPIO_NUM;

  config.pin_sscb_scl = SIOC_GPIO_NUM;

  config.pin_pwdn = PWDN_GPIO_NUM;

  config.pin_reset = RESET_GPIO_NUM;

  config.xclk_freq_hz = 20000000;

  config.pixel_format = PIXFORMAT_JPEG;

  //init with high specs to pre-allocate larger buffers

  if(psramFound()){

    config.frame_size = FRAMESIZE_UXGA;

    config.jpeg_quality = 10;

    config.fb_count = 2;

  } else {

    config.frame_size = FRAMESIZE_SVGA;

    config.jpeg_quality = 12;

    config.fb_count = 1;

  }

#if defined(CAMERA_MODEL_ESP_EYE)

  pinMode(13, INPUT_PULLUP);

  pinMode(14, INPUT_PULLUP);

#endif

  // camera init

  esp_err_t err = esp_camera_init(&config);

  if (err != ESP_OK) {

    Serial.printf("Camera init failed with error 0x%x", err);

    return;

  }

  sensor_t * s = esp_camera_sensor_get();

  //initial sensors are flipped vertically and colors are a bit saturated

  if (s->id.PID == OV3660_PID) {

    s->set_vflip(s, 1);//flip it back

    s->set_brightness(s, 1);//up the blightness just a bit

    s->set_saturation(s, -2);//lower the saturation

  }

  //drop down frame size for higher initial frame rate

  s->set_framesize(s, FRAMESIZE_QVGA);

#if defined(CAMERA_MODEL_M5STACK_WIDE)

  s->set_vflip(s, 1);

  s->set_hmirror(s, 1);

#endif

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {

    delay(500);

    Serial.print(".");

  }

  Serial.println("");

  Serial.println("WiFi connected");

  startCameraServer();

  Serial.print("Camera Ready! Use 'http://");

  Serial.print(WiFi.localIP());

  Serial.println("' to connect");

}

void loop() {

  // put your main code here, to run repeatedly:

  delay(10000);

}

```

**COMPONENTS REQUIRED FOR DOORBELL**
- ESP32-CAM
- FTDI Programmer
- Buzzer
- Push Button
- LED 
- 3.7V Battery

![tag](../img/ww.jpg)

**CIRCUIT DIAGRAM**

![tag](../img/aal9.png)

---
Circuit diagram for this Smart Wi-Fi Doorbell is very simple, just connect the LED, a push button and a buzzer to **ESP32 GPIO pins**. A **buzzer** is used to make a sound whenever the button is pressed. The **LED** is used to indicate the network status. Network LED will be in a high state if ESP is connected to a network otherwise, it will **blink**.

1 |  2
--- | ---
![tag](../img/dev6.jpg) | ![tag](../img/dev7.jpg)


**IFTTT SETUP FOR Wi-Fi DOORBELL**
---
**IFTTT** is a free web-based service that allows users to create chains of simple conditional statements, called “recipes”, which are triggered based on changes to other web services such as Gmail, Facebook, Instagram, and Pinterest. IFTTT is an abbreviation of “If This Then That”.

In this project, IFTTT is used to send an Email, SMS as an event to stream.

---
- First login to [IFTTT](https://ifttt.com/login) with your credentials or Sign Up if you don’t have an account on it.

---
- Now search for **‘Webhooks’** and click on the Webhooks in Services section.

![tag](../img/aal17.png)

---
- Now, in the Webhooks window, click on **‘Documentation’** in the upper right corner to get the private key.

---
- Copy this key. It will be used in the program.

![tag](../img/aal18.png)

---
- After getting the private key, now we will create an applet using Webhooks and Email services. To create an applet click on **‘Create.’**

![tag](../img/aal19.png)

---
- Now in the next window, click on the **‘This’** icon.

![tag](../img/aal20.png)

---
- Now search for Webhooks in the search section and click on **‘Webhooks.’**

![tag](../img/aal21.png)

---
- Now choose **‘Receive a Web Request’** trigger and in the next window, enter the event name as **button_pressed** and then click on create a trigger.

---
- Now to complete the applet, click on **‘That’** to create a reaction for the **button_pressed** event.

![tag](../img/aal22.png)

---
- Here we will play a specific song on the phone when the IoT doorbell button is pressed. For that search for **‘Android device’** in the search section.

![tag](../img/aal23.png)

---
- Now in Android Device, choose **‘Play a specific song’** trigger.

![tag](../img/aal24.png)

---
- Now enter the song title that you want to play when the doorbell button is pressed.
After that, click on **‘Create action’** and then **‘Finish’** to complete the process.

---
- Now create another applet to send a message with the webpage link to the phone when the doorbell button is pressed.

---
- So to create this applet choose **‘Webhooks’** in **‘this’** section and in **‘that’** section choose **Android SMS**.

![tag](../img/aal25.png)

---
- Now it will ask to enter the phone number and message body. For this doorbell project, i am sending a message with the Webserver link so that you can see the live video streaming directly.

![tag](../img/aal26.png)

**CODE EXPLANATION**

**Note**: Use the webserver example code and add this

First, **include all the required library files** for this code.

```ruby

#include "esp_camera.h"
#include <WiFi.h>

```

Then enter the **Wi-Fi credentials.**

```ruby

const char* ssid = "Wi-Fi Name";
const char* password = "Wi-Fi Password";

```

After that, I entered the **IFTTT** hostname and private key that you copied from the IFTTT website.

```ruby

const char *host = "maker.ifttt.com";
const char *privateKey = "nPRe9wjOuyE_JJAwJ5pIj7x6NH57_N38z0_Mj0A8puV";

```

**Define** all the pins that you are using in this project. I’m using the GPIO 2, 13 and 15 pins to connect the push button, LED and buzzer.

```ruby
const int buttonPin = 2;
const int led1 = 13;
const int buzzer = 15;

```
I want to send it a fixed IP to me so i created the **void startCameraServer** 

```ruby
  // it wil set the static IP address to 192, 168, 1, 184
  IPAddress local_IP(192, 168, 1, 184);
  //it wil set the gateway static IP address to 192, 168, 1,1
  IPAddress gateway(192, 168, 1, 1);

  // Following three settings are optional
  IPAddress subnet(255, 255, 0, 0);
  IPAddress primaryDNS(8, 8, 8, 8); 
  IPAddress secondaryDNS(8, 8, 4, 4);

``` 

Inside the **void setup** loop, define the button pin as input and LED and buzzer pins as output.

```ruby

void setup() {
  pinMode(buttonPin, INPUT);
  pinMode(led1, OUTPUT);
  pinMode(buzzer, OUTPUT);

```
It will try to connect to Wi-Fi using the given credentials, and when connected to a network LED state will change from low to high.

```ruby

WiFi.begin(ssid, password);
  int led = LOW; 
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
    digitalWrite(led1, led);
    led = !led;
  }
  Serial.println("");
  Serial.println("WiFi connected");
  digitalWrite(led1, HIGH);

```

While disconnected from a network ESP32 will restart until it connects to a network.

```ruby

while (WiFi.status() == WL_DISCONNECTED) {
    ESP.restart();
    digitalWrite(led1, LOW);
    Serial.print("Connection Lost");

```

ESP32 will read the button state, and if the button is in the LOW state (pulled high), i.e., a button has been pressed, it sends the event and turns on the buzzer for 3 seconds.

```ruby

int reading = digitalRead(buttonPin);
if (buttonState == LOW) {
        send_event("button_pressed");
        Serial.print("button pressed");
        digitalWrite(buzzer, HIGH);
        delay(3000);
          digitalWrite(buzzer, LOW);

```
**NGROK**
---
**Ngrok**, a very cool, lightweight tool that creates a secure tunnel on your local machine along with a public URL you can use for browsing your local site. When ngrok is running, it listens on the same port that you're local web server is running on and proxies external requests to your local machine.

- Sign up for ngrok online and download and install ngrok on windows or pi os [https://dashboard.ngrok.com/get-started/setup](https://dashboard.ngrok.com/get-started/setup)


1 |  2
--- | ---
![tag](../img/al1.png) | ![tag](../img/al2.png)


---
- **Authotokens** : Copy Token from ngrok online account and add it to your ngrok local with command below.

![tag](../img/al3.png)

>>>
ngrok 1hNGN7KWhrQ9dByI4jyvHuw3rzP_6FNFmuhNNJC9sKog8WhRq
>>>

---
- Upload your code and find your IP address

![tag](../img/al4.png)

---
- Run this command: >ngrok http (**your IP**) 

![tag](../img/al6.png)

- Access ngrok via : [http://736ca4d15270.ngrok.io/stream](http://736ca4d15270.ngrok.io/stream)

**NOTE : do not forget to add stream to access stream!**

![tag](../img/al7.png)

- Stream public

![tag](../img/al8.jpeg)

---

**3D PRINTING**
---
- Here I designed a 3D printed casing for this doorbell. I used **Thinkercad**

**THE CASING**
---
![tag](../img/sc.PNG)
 
**THE STANDER** 
---
![tag](../img/sc2.PNG)

- After that, I exported it as an STL file, sliced it based on printer settings in **CURA**, and finally printed it.

![tag](../img/sc1.PNG)

---

1 |  2
--- | ---
![tag](../img/dev3.jpg) | ![tag](../img/dev44.jpg)


**THIS IS HOW IT LOOKS AFTER IT'S DONE**
---

1 |  2
--- | ---
![tag](../img/dev1.jpg) | ![tag](../img/dev2.jpg)


**TESTING**
---
Now whenever the IoT doorbell button is pressed the smartphone starts playing a song, and a message will be received with a link of a webpage as shown below. 

![tag](../img/dev5.jpg)

## References

**CODE**

<a href="myFile.js" download>Doorbell</a>

**STL FILES**

<a href="myFile.js" download>Stander</a>

<a href="myFile.js" download>Casing</a>

## Links

[Presentation Slide](https://docs.google.com/presentation/d/1p9hb_un-i3Vats08UJ-YdSd4wF94hnaFIsxfTGB6u1A/edit?usp=sharing)

[Business Canvas](https://canvanizer.com/canvas/rNnPfECJRVmkT)




