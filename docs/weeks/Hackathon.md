## Caribbean Girls 3D Virtual Hackathon

The Caribbean Girls 3D Virtual Hackathon was all about technocreativity.
Challenging over 700 girls around the region to develop tech solutions to pressing regional issues. With covid it was an 3D Virtual Hackathon.

One week before the first pitch our team was invited to participate. 

- **Team Name** : **_Fusion Girls_**

![tag](../img/f2.jpg)

- We chose the theme **Blue economy:''Save OUr Oceans''**


**CHALLENGE**

Make an animation video!

**GearTracks**

GearTracks aims at finding a preventive solution for "ghost gear", lost gear ending up in the oceans, which results in loss of thousands of dollars and damage to marine life. Instead of finding solutions after the damage has occurred we provide you with a framework to prevent the problems from even starting. While most focus on tracking and monitoring vessels we aim to track the fishing gear which is the actual source of the problem.

**SOLUTION**

- GearTracks is a gear tracking and monitoring framework based on incorporating a low cost Internet of Things Solution for fisheries.
- GearTracks provides users with devices called flutters and a dashboard with which they can monitor the usage, state, and position of their fishing gear. It is built in such a way to be "set and forget" and unobtrusive to the normal fisheries process.
- Flutters consists of an esp32 microcontroller capable of both wifi and bluetooth. Attached to the esp32 is a pressure sensor used to detect the state of the flutter device. When the status under water is detected the esp32 will trigger the emploding of the co2 capsule causing the flutter to float to the surface.
- The status is sent to the fisherman over either bluetooth or wifi.

**GearTracks Flutters**

Flutters come in two types, one for fishnets and one for crab cages. Our prototype was built mostly from recycled products like PVC tubes and fittings combined with generally available consumables like CO2 tanks and braided high strength fish line. Flutters connect locally to their IoT gateway (raspberry pi) which also hosts the dashboard and connects to the internet.Users can locally monitor and store their data until connectivity to the internet is available where their data will be stored in the cloud.

---

![tag](../img/deqw.PNG)

---

- **For the animation video our group made mockups** 

![tag](../img/f4.PNG)

- Used Adobe Premiere Pro to edit our video and add gifs to it.
- Also we created an stop motoion video with sketches which we colored with Adobe Photoshop

1 |  2
--- | ---
![tag](../img/Turtle 59.jpg)  | ![tag](../img/Turtle 62.jpg)
---

After pitching in the first round we were selected for the second round.
One hundred and fifty girls, representing 36 teams, from 10 Caribbean countries participated in the finals, pitching their techno creative solutions under three themes: Climate Change, Save Our Oceans and Gender-Based Violence (GBV), using the modalities of Animation, Short Film, Music Video, Photo Story Board, and Vlog to bring their creative concepts to life.

---

The event culminated in the highly anticipated announcement of the Hackathon 2021 winners
The teams from Suriname, **The Girl Code and Fusion Girls**, copped both the first and second place spots, with Animation stories on Climate Change and Save Our Oceans 

1 |  2
--- | ---
![tag](../img/1st.jpg)  | ![tag](../img/2nd.jpg)


----


## Group Picture

![tag](../img/cc.jpg)


## Link to presentation slide and animation video


[Presentation Slide](https://docs.google.com/presentation/d/1wONQyD4DOnFocwTZsBl3q00Zcf7nsfvZTpB9g8CyyJs/edit?usp=sharing)

[Animation video](https://www.youtube.com/watch?v=cm3aU3uhD7s)

