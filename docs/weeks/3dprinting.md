### What is 3D Printing? 

3D printing or additive manufacturing is a process of making three dimensional solid objects from a digital file.

The creation of a 3D printed object is achieved using additive processes. In an additive process an object is created by laying down successive layers of material until the object is created. Each of these layers can be seen as a thinly sliced cross-section of the object.

3D printing is the opposite of subtractive manufacturing which is cutting out / hollowing out a piece of metal or plastic with for instance a milling machine.

3D printing enables you to produce complex shapes using less material than traditional manufacturing methods.

### 3D Printing Software

**CURA**

Cura is an open source slicing application for 3D printers

----

**During my journey the printers I used were:** 

- **DeltaBot:** is a consumer 3D printer that uses DeltaBot technology (three robotic arms attached to a base). The Deltabot uses a print head attached to 3 sets of robotic arms to deposit the material.

**How the DeltaBot 3D Printer Works**

First, the user creates a 3D model using 3D modeling software and saves it as an STL file. The file is sent to Cura.The model is sent to the printer. ABS or PLA plastic extrudes through a print head which is attached to 3 sets of robotic arms that connect to 3 pillars that surround the build platform. This ensures that the build platform stays level. The print head prints the part layer by layer until the part is complete. The print head can be moved to any X, Y, and Z position on the build platform.

---

### Here are the machine settings DeltaBot and For Printing in PLA

![tag](../img/delta.png)

---

### Cura

![tag](../img/delta1.png)

--- 

### Printing 

![tag](../img/dev3.jpg)

---

**The second Printer I used was:**

- **Anycubic 4 Max Pro:** Enclosed Printing space with larger build volume enhance the printing quality, out of box, no need assemble, optional auto power off feature is safer for children. Patented printing platform with excellent adhesion whilst printing and pop off once it cools down. The sensor triggers the machine to pause prints and set off an alarm when filament run out.

**Technical Specifications**

- Printing Technology: FDM (Fused Deposition Modeling)
- Layer Resolution: 0.05-0.3 mm
- Positioning Accuracy: X/Y 0.0125mm Z 0.002mm
- Supported Print Materials: PLA, TPU,ABS, HIPS, Wood
- Print Speed: 20~80mm/s (suggested 50mm/s)
- Nozzle Diameter: 0.4 mm/1.75 mm
- Build Size:270mm(L)×205(W)×205mm(H)
- Operational Extruder Temperature: Max 260ºC
- Operational Print Bed Temperature: Max 100ºC
- Extruder Quantity: Single
- Input Formats: .STL, .OBJ, .DAE, .AMF
- Ambient Operating Temperature: 8ºC - 40ºC
- Connectivity SD Card, USB Port (expert users only)
- Package Weight: 18.5kg

---

### settings

---

### Cura

![tag](../img/any1.jpg)

---

### Printing

![tag](../img/any.jpg)
