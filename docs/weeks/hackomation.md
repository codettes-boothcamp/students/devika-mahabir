**Hack O'mation 2020 Final project**

The Internet of Things (IoT) is a computing concept that describes a  future where everyday physical objects will be connected to the Internet  and be able to identify themselves to other devices, exchange data without  requiring human-to-human or human-to-computer interaction. 

**Hack O’mation Format:**

- Pre-phase (4 weeks): Open Sessions to all participants:  IoT intro, Brainstorm & Idea-building, Teambuilding, Pitching  Embedded and UI programming sessions.
- Idea Pitching to Jury (first scoring)
- Main Event (6-8 weeks):  Prototype building in teams   Weekly support meetups (professionals in mechanics, electronics,  programming etc)
- Final presentation of projects June 2020

**Top 3 projects selected by juryHack O’mation Tech:**
- Control something, connect to it over the internet, read-out sensor(s) data and
- Control actuator(s) preferrably using an API- Hardware based around RaspberryPi and Arduino as controllers
- Use any software-tools/frameworks you want
- Sensor-pack available (40 sensors), and special sensors and/or actuators will be  ordered after approval of your project
- Proto-boards available; your custom PCB can be printed/etched or milled also
- Project must be feasible within time-frame (8-12 weeks) of challenge

**Support available for:** 
- Electronics), Software/IO control, Manufacturing  of parts & Mechanics
- Product prototype MUST FUNCTION to be qualified for judging!

Hack O'mation theme for 2020: **Sustainable Tourism Suriname &Health**

Everything that solves tourism’s (in the interior) problems:
- The product
- Facilities & comfort
- Communication
- Safety & health
- Experience.

**Group Codettes bootcamp 2 participants:** 
- Shewishka
- Anjessa
- Devika
- Drishti
- Disha

Group name: **leadHers**

Project Hack O'mation: **hydropower plant**

Electricity can be generated in different ways. One of the ways is through hydropower. We are building a small bluewater Hydropower that can be use by everyone. It will be inexpensive so everybody can buy it. This will be use to generate electricity for small devices when you are outdoor. This can be very usefull when you are at a place with no electricity. It has a magnet coupling so it is easy to switch propellers.

**Logo:** 

**General Problem:** No Electricity outdoors. 
For tourists: Most of the time when tourists go camping or there is no electricity. You start panicking about a low battery and want to charge your phone. And there is no electricity.

**Solutions**
Building a water tribune that will generate electricity through the flow of water. It can charge small devices easily. And it is nature friendly. 

**Features:**
- 24 hour of power
- It stores energy
- The product is portableUnique Values
- Affordable IoT Solutions
- Convenience: It is easy to use
- It is small so easy to transport

**The working Process**
First we started to research about our product and how we can make it. We searched how it will look and what we will need to make it. We put our mind to it and planned to use a DC motor to make our propeller rotate. 

**For making our portable water turbine we used the following components:**
- diodes
- 5v regulator
- dc motor
- charge circuit
- esp32(we are using the ttgo) for the dashboard

We try to build the diode bridge and try to let it work with the dc motor: