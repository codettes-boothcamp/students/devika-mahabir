**INTERFACE**

**WEEK 1**

**The Raspberry Pi** is a low cost, credit-card sized computer that plugs into a computer monitor or TV, and uses a standard keyboard and mouse.
It is a capable little device that enables people of all ages to explore computing, and to learn how to program in languages like Scratch and Python.
It’s capable of doing everything you’d expect a desktop computer to do, from browsing the internet and playing high-definition video,
to making spreadsheets, word-processing, and playing games. test

- **How to find my Laptop Adress** 

Search on your laptop **Command Prompt**

Type: **ipconfig** 

![tag](../img/63.png)

- **TOOLS**
*  Raspberry Pi
*  Ethernet or Cross Cables
*  Sd Card 16gb min
*  Power Cable micro usb
*  Laptop ethernet slot 

![tag](../img/34.png)

- **BASIC RASPBERRY PI SETUP**
1.   Download Raspbian version (Jessie or Stretch)
2.   Install Disk32Imager
3.   Install Putty
4.   Install Winscp
5.   Install Notepad++
6.   Install Angry ip scanner

**NOTE:** I used Raspberry Pi 2 so **Raspbian Jessie**.

- **Insert SD card in laptop**

- Write **Raspbian Jessie** on SD card using **Win32DiskImager**.

![tag](../img/35.png)

- Select Raspberry Pi on SD and add an empty file **ssh**

![tag](../img/38.png)

- Edit the file **cmdline.txt** on **Notepad++**

![tag](../img/37.png)

- Type in IP adress **192.168.1.169"**
Save the file and move/insert SD card into Raspberry Pi and connect to that **IP adress**. 

![tag](../img/36.png)

- Open Putty and type in the **IP adress**, select **SSH**, give it a name and **save**.

![tag](../img/put.png)

![tag](../img/putt.png)

- **Login**

User: **pi**

Password: **raspberry**

Type in: **sudo raspi-config**

![tag](../img/log.png)

- **Configuration**

- **Up down left right** keys on keyboard to navigate. 

![tag](../img/nav.png)

**WEEK 2** 

**DAY 1**

**Installing Nodejs and Python on Raspberry Pi**

**What is Node.js?**

- Node.js is an open source server environment
- Node.js is free
- Node.js runs on various platforms (Windows, Linux, Unix, Mac OS X, etc.)
- Node.js uses JavaScript on the server

**Express** is a web application framework for Node.js

**Basic Linux Commands**

1.  cd - navigate through directories 
2.  cd  .. go back to root directory 
3.  ls	- list  
4.  dir	-directories
5.  mkdir - make directory 
6.  touch index.js - make text file
7.  rm	- remove files
8.  rmdir- remove directories 

![tag](../img/ls.png)

**Nodejs folder structure**

>**nodejs**
>>**projects**
>>>**project1**
>>>>- index.js
>>>>>**public**
>>>>>>- index.html
>>>>>>>**css**
>>>>>>>>- style.css
>>>>>>>>>**package.json**

![tag](../img/no.png)

- **Open putty and check all the versions you have.**

![tag](../img/ver.png)

**To install Nodejs**

- curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash - sudo apt-get install nodejs

All npm packages contain a file, usually in the project root, called **package.json**- this file holds various metadata relevant to the project.
This file is used to give information to npm that allows it to identify the project as well as handle the project's dependencies.
It can also contain other metadata such as a project description, the version of the project in a particular distribution, license information,
even configuration data - all of which can be vital to both npm and to the end users of the package.
The package.json file is normally located at the root directory of a Node.js project.

- Execute the following command **npm init -y** in your **project1** folder to initialize your Node.js project with default settings. 

Then, under the project directory, create the entry point of the application, a file named **index.js**.

![tag](../img/npmm.png)

- You'll use **nodemon** to monitor your project source code and automatically restart your Node.js server whenever it changes.

Install **nodemon** => **npm i -D nodemon**

![tag](../img/dem.png)

- To use **express** you have to install it first. The command is **npm i express**.

![tag](../img/ex.png)

- Open **package.json** and delete the **test** script and create a **dev** script in your command.

~~~
    {
      "name": "whatabyte-portal",
      "version": "1.0.0",
      "description": "",
      "main": "index.js",
      "scripts": {
        "dev": "nodemon ./index.js"
      },
      "keywords": [],
      "author": "",
      "license": "ISC",
      "devDependencies": {
        "nodemon": "^1.19.3"
      }
    }

~~~

- Open **index.js** and populate it with the following template that defines the core structure of an Express application

~~~javascript
    // index.js
    
    /**
     * Required External Modules
     */
    
    /**
     * App Variables
     */
    
    /**
     *  App Configuration
     */
    
    /**
     * Routes Definitions
     */
    
    /**
     * Server Activation
     */

~~~
 
 
-  Now **edit** the code


~~~javascript
    // index.js
    
    /**
     * Required External Modules
     */
    
    const express = require("express");
    const path = require("path");
    
    /**
     * App Variables
     */
    
    const app = express();
    const port = process.env.PORT ||"8000";
    
    /**
     *  App Configuration
     */
    
    
    app.use(express.static(path.join(__dirname, "public")));
    
    /**
     * Routes Definitions
     */
    
    
    
    /**
     * Server Activation
     */
    
    app.listen(port, () => {
      console.log(`Listening to requests on http://localhost:${port}`);
    });
    
    
~~~


- Under the **Required External** section, import the **express** and **path** folders.
- Under the **App Variables** section you will execute the default function exported by the **express**
in **app** and define the **port** your server will use to listen as requested.
- Under the **App Configuration** you will define the **public** folder where you put everything (like your images etc).
- Under the **Server Activation** section start a server listening for incoming requests on your port and to display a message to confirm it's listening.

**NOTE** : Under the **Routes Definitions** section it's **blank!!**

- Now open your **public** folder and add the **basic HTML**


~~~html
    <html>
    <body>
    <h1>My website</h1>
    </body>
    </html>
    
~~~




- Finally execute **dev** to test the script and run the app => **npm run dev**.

![tag](../img/dev.png)

- Go to your browser and type in your **IP adress:PORT**. (**e.g.** Mine is **192.168.1.169:8000**) and visit your website

![tag](../img/65.png)

**NOTE** : You can edit your website using **Javascript, HTML and CSS**.

**DAY 2**

We will set up a web server and create a simple website using **Flask, Phyton and HTML/CSS**.

- **Python** is an interpreted, high-level, general-purpose programming language. 
- **Flask** is a micro web framework written in Python.
- **HTML (Hypertext Markup Language)** is the standard markup language for documents designed to be displayed in a web browser.
- **CSS (Cascading Style Sheets)** is a style sheet language used for describing the presentation of a document written in a markup language like HTML.

**Install Python**
- Install Python 2.7 
- Install pip
- Install flask using **sudo pip install flask**

**Python Folder Structure**

>**python**
>>- Index.py
>>>**static**
>>>>- img
>>>>>**templates**
>>>>>>- index.html

![tag](../img/py1.png)

![tag](../img/py2.png)

- Open **index.py** and copy the code




~~~python
    from flask import Flask, render_template
    
    app = Flask(__name__)
    
    @app.route('/')
    def index():
        return render_template('index.html')
    if __name__ == '__main__':
        app.run(debug=True, host='0.0.0.0')
        
    
~~~





- **@app.route('/')**: this determines the entry point; the / means the root of the website, so **http://127.0.0.1:5000/**
- **def index()**: this is the name you give to the route; this one is called **index**, because it’s the index (or home page) of the website.

- Now enter the command **python index.py**

![tag](../img/py3.png)

- Open **index.html** and copy the **basic HTML**.




~~~html
    <html>
    <body>
    <h1>My website</h1>
    </body>
    </html>


~~~




![tag](../img/67.png)

**ARDUINO UNO SERIAL COMMUNICATION**

- Build the circuit

![tag](../img/py4.png)

- Open Arduino and test it out with **basics** - **blink**.
- After the test Connect your **Arduino** to your **Raspberry Pi**

![tag](../img/py6.jpg)

- And the **examples** - **communication** - **PhysicalPixel**

![tag](../img/py7.png)

- Create a **Pyhton** file 
- Give it any name with **extension .py** 
- And then **sudo pip install pyserial**

![tag](../img/py8.png)

- Type **lsusb** to see a list of your **usbports** on which your Arduino UNO is connected.
- **dmesg | grep tty** to see the serial port for your Raspberry Pi, if using Raspberry Pi 2 then **/dev/ttyAMA0**

- Open your file and add the code




~~~python
    import serial
    import time
    # Define the serial port and baud rate.
    # Ensure the 'COM#' corresponds to what was seen in the Windows Device Manager
    
    ser = serial.Serial('/dev/ttyACM0', 9600)   # ttyACM0 serial port for pi 2 #/dev/ttyAMA0 pi 3
    def led_on_off():
        user_input = input("\n Type on / off / quit : ")
        if user_input =="on":
            print("LED is on...")
            time.sleep(0.1) 
            ser.write(b'H') 
            led_on_off()
        elif user_input =="off":
            print("LED is off...")
            time.sleep(0.1)
            ser.write(b'L')
            led_on_off()
        elif user_input =="quit" or user_input == "q":
            print("Program Exiting")
            time.sleep(0.1)
            ser.write(b'L')
            ser.close()
        else:
            print("Invalid input. Type on / off / quit.")
            led_on_off()
    
    time.sleep(2) # wait for the serial connection to initialize
    
    led_on_off()


~~~



- Finally to make it work type **sudo python (name).py** (**e.g.** Mine is **spy.py** so **sudo python spy.py**)

![tag](../img/py9.png)

- When you type **on** the Led will turn **On**

![tag](../img/py10.jpeg)

- When you type **off** the Led will turn **Off**

![tag](../img/py11.jpeg)


**HTLM/CSS/JavaScript**

**Day 1**


*  Learn to type code!
*  Learn HTML5/CSS3
*  Build your own basic website

![tag](../img/147.PNG)

**HTML or HyperText Mark-up** Language is a language to produce visual output on a website or modern web-app. The HTML5 standard, the latest version, was released by the W3C Consortium in 2014. 
W3C is a group that standardizes the world wide web languages and protocols to enable streamlined experience and integration between all browsers, and back-ends of participating members.

**Some basic tags**

~~~

1. <B>FOR BOLD</B>
2. <H1> FOR HEADING1</H1>
3. <BR> FOR BREAK (go to the next line)
4. <img src="img/iot.jpg"/>

~~~

**NEW Semantic TAGS that actually have more meaning to the standard**

~~~javascript
<DIV> tags for layout.
For layout : <article>, <header>, <footer>, <nav>, <aside> 
For widgets: <meter>, <progress> etc
~~~

**Steps before u create website**


- **Determine Message/Goal/Target audience (why the website)**
- **Make a site structure / site map of pages and sections of website**
- **Setup website folder structure**

The root folder is **Public**

>**public**
>>- index.html
>>>**img**
>>>>**css**
>>>>>**fonts**
>>>>>>**js**
>>>>>>>- lib


- **Collect content**

Once in the process of building a site it is best to have all content ready and available as much as possible.
It gives an idea of the amount of information for each webpage, sometimes forcing us to split it up or at least give it a substructure to make it more readable.

- **Select the look & feel**

Get an idea up front on how you want to display your information

- **Build Mockups**

**Mockup to HTML layout**

![tag](../img/148.PNG)

- **Target devices**

Decide what the target device(s) will be and if you want to support them all at once. This requires you to build Responsive site.

**HTML base structure**

The basics of a HTML5 is its fixed structure that will be standard for each page. Any browser should be able to understand what to do if one sticks to this structure.
To make code more readable apply tabbed nesting of hierarchical elements (like below)

~~~ javascript
!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Title MyPage (shows in browser window)</title>
</head>
<body>
		<!-- body content starts here -->
Here is the visible Content of the page......
</body>
</html>
~~~

To tie everything together i will create an index.html file in the root folder of the website.
In this file is the master layout for the website, with menus, side-bar, header and footer for showing all sub-pages. 

**The code**

~~~
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title>Title</title>
    <link href="css/style.css" rel="stylesheet" />
    <meta name ="viewport" content ="width=device-width, initial-scale=1.0">
</head>

<body>
    <header class ="mainHeader">
    	<img src="img/logo.png">
	<nav>
	      <ul>
	            <li><a href="#" class="active">Home</a></li>
	            <li><a href="#">About</a></li>
	            <li><a href="#">Portfolio</a></li>
	            <li><a href="#">Gallery</a></li>
	            <li><a href="#">Contact</a></li>
	  	</ul>
        </nav>
    </header>
    
    <div class="mainContent">
	    <div class="content">
    		<article class="articleContent">
	            <header>
	                <h2>First Article #1</h2>
	            </header>
	            
	            <footer>
	            	<p class="post-info">Written by Super Woman</p>	            	
	            </footer>
	            <content>
	            	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        		</content>
    		</article> 
    		
    		<article class="articleContent">
	            <header>
	                <h2>2nd Article #2</h2>
	            </header>
	            
	            <footer>
	            	<p class="post-info">Written by Super Woman</p>	            	
	            </footer>

	            <content>
	            	<p>This is the actual article content ... in this case a teaser for some other page ... read more</p>
        		</content>
    		</article> 
    		
	   </div>
	</div>
	
	<aside class="top-sidebar">
		<article>
			<h2>Top sidebar</h2>
			<p>Lorum ipsum dolorum on top</p>
		</article>
	</aside>
	
	<aside class="middle-sidebar">
		<article>
			<h2>Middle sidebar</h2>
			<p>Lorum ipsum doloru in the middle</p>
		</article>
	</aside>
	
	<aside class="bottom-sidebar">
		<article>
			<h2>Bottom sidebar</h2>
			<p>Lorum ipsum dolorum at the bottom</p>
		</article>
	</aside>
	
	<footer class="mainFooter">
		<p>Copyright &copy; <a href="#" title = " MyDesign">mywebsite.com</a></p>
	</footer>
		
</body>

</html>

~~~


![tag](../img/149.png)


**CSS3 or  Cascading Style Sheets** enables the separation of presentation and content, including layout, colors, and fonts.[3]
To improve content accessibility, more flexibility and control of presentation characteristics,web pages share formatting by specifying the relevant CSS in a 
separate css file.

Create the style/style.css file and simply assign colors to each of the above mentioned sections and add the reference to this stylesheet inside your HTML page <head> section
with the following tag:

~~~
<link href="css/style.css" rel="stylesheet" />
~~~

**The resulting CSS code**

~~~javascript

/* 
	Stylesheet for:
	HTML5-CSS3 Responsive page
*/

/* body default styling */

body {
    /* border: 5px solid red; */
    background-image: url(img/bg.png);
    background-color: #000305;
    font-size: 87.5%;
    /* base font 14px */
    font-family: Arial, 'Lucinda Sans Unicode';
    line-height: 1.5;
    text-align: left;
    margin: 0 auto;
    width: 70%;
    clear: both;
}


/* style the link tags */
a {
    text-decoration: none;
}

a:link a:visited {
    color: #8B008B;
}

a:hover,
a:active {
    background-color: #8B008B;
    color: #FFF;
}


/* define mainHeader image and navigation */
.mainHeader img {
    width: 30%;
    height: auto;
    margin: 2% 0;
}

.mainHeader nav {
    background-color: #666;
    height: 40px;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}

.mainHeader nav ul {
    list-style: none;
    margin: 0 auto;
}

.mainHeader nav ul li {
    float: left;
    display: inline;
}

.mainHeader nav a:link,
mainHeader nav a:visited {
    color: #FFF;
    display: inline-block;
    padding: 10px 25px;
    height: 20px;
}

.mainHeader nav a:hover,
.mainHeader nav a:active,
.mainHeader nav .active a:link,
.mainHeader nav a:active a:visited {
    background-color: #8B008B;
    /* Color purple */
    text-shadow: none;
}

.mainHeader nav ul li a {
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}


/* style the contect sections */

.mainContent {
    line-height: 20px;
    overflow: none;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}

.content {
    width: 70%;
    float: left;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}

.articleContent {
    background-color: #FFF;
    padding: 3% 5%;
    margin-top: 2%;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}

.post-info {
    font-style: italic;
    color: #999;
    font-size: 85%;
}

.top-sidebar {
    width: 21%;
    margin: 2% 0 2% 3%;
    padding: 3% 5%;
    float: left;
    background-color: #FFF;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}

.middle-sidebar {
    width: 21%;
    margin: 2% 0 2% 3%;
    padding: 3% 5%;
    float: left;
    background-color: #FFF;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}

.bottom-sidebar {
    width: 21%;
    margin: 2% 0 2% 3%;
    padding: 3% 5%;
    float: left;
    background-color: #FFF;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}

.mainFooter {
    width: 100%;
    height: 40px;
    float: left;
    border-color: #666;
    margin: 2% 0;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}

.mainFooter p {
    width: 92%;
    margin: 10px auto;
    color: #FFF;
}

~~~

**Adding Chart to website**
 
Download chart and add to folder:

>**Public**
>>**Js**
>>>**lib**

In **index.html** add code to host chart inside your 
**head**

~~~
<script src="js/lib/Chart.js/Chart.js"></script>
<script src="js/utils.js"></script>
~~~

Add to  **Article 2** in your **body**:

~~~
<div style="width:100%; height:100%">
	<canvas id="canvas"></canvas>
	<button id="randomizeData">Randomize Data</button>
	<button id="addDataset">Add Dataset</button>
	<button id="removeDataset">Remove Dataset</button>
	<button id="addData">Add Data</button>
	<button id="removeData">Remove Data</button>
</div>
~~~

Add Javascript at bottom just above the **body**

~~~
<script>
	var MONTHS = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
	var config = {
		type: 'bar',
		data: {
			labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
			datasets: [{
				label: 'My First dataset',
				backgroundColor: window.chartColors.red,
				borderColor: window.chartColors.red,
				data: [
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor()
				],
				fill: false,
			}, {
				label: 'My Second dataset',
				fill: false,
				backgroundColor: window.chartColors.blue,
				borderColor: window.chartColors.blue,
				data: [
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor()
				],
			}]
		},
		options: {
			responsive: true,
			title: {
				display: false,
				text: 'Chart.js Line Chart'
			},
			tooltips: {
				mode: 'index',
				intersect: false,
			},
			hover: {
				mode: 'nearest',
				intersect: true
			},
			scales: {
				xAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: 'Month'
					}
				}],
				yAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: 'Value'
					}
				}]
			}
		}
	};

	window.onload = function() {
		var ctx = document.getElementById('canvas').getContext('2d');
		window.myLine = new Chart(ctx, config);
	};

	document.getElementById('randomizeData').addEventListener('click', function() {
		config.data.datasets.forEach(function(dataset) {
			dataset.data = dataset.data.map(function() {
				return randomScalingFactor();
			});

		});

		window.myLine.update();
	});

	var colorNames = Object.keys(window.chartColors);
	document.getElementById('addDataset').addEventListener('click', function() {
		var colorName = colorNames[config.data.datasets.length % colorNames.length];
		var newColor = window.chartColors[colorName];
		var newDataset = {
			label: 'Dataset ' + config.data.datasets.length,
			backgroundColor: newColor,
			borderColor: newColor,
			data: [],
			fill: false
		};

		for (var index = 0; index < config.data.labels.length; ++index) {
			newDataset.data.push(randomScalingFactor());
		}

		config.data.datasets.push(newDataset);
		window.myLine.update();
	});

	document.getElementById('addData').addEventListener('click', function() {
		if (config.data.datasets.length > 0) {
			var month = MONTHS[config.data.labels.length % MONTHS.length];
			config.data.labels.push(month);

			config.data.datasets.forEach(function(dataset) {
				dataset.data.push(randomScalingFactor());
			});

			window.myLine.update();
		}
	});

	document.getElementById('removeDataset').addEventListener('click', function() {
		config.data.datasets.splice(0, 1);
		window.myLine.update();
	});

	document.getElementById('removeData').addEventListener('click', function() {
		config.data.labels.splice(-1, 1); // remove the label first

		config.data.datasets.forEach(function(dataset) {
			dataset.data.pop();
		});

		window.myLine.update();
	});
</script>

~~~


![tag](../img/119.png)

**JavaScript**

**JavaScript** was initially created to “make web pages alive”.
With NodeJS now also server-side or on any device that has a special program called the JavaScript engine.
It's a Functional Programming style.

**What can it do?**

- Add new HTML to the page, change the existing content, modify styles.
- React to user actions, run on mouse clicks, pointer movements, key presses.
- Send requests over the network to remote servers, download and upload files (so-called AJAX and COMET technologies).
- Get and set cookies, ask questions to the visitor, show messages.
- Remember the data on the client-side (“local storage”)

**What can’t it do?**

- Directly talk to your computer or device functions
- Is NOT allowed to communicate freely between sites
- One browser window does NOT know of any other browser window/tab
- Cross domain communication crippled
- Be compiled so all code is visible to others
- Is considered insecure

**Using Objects and Arrays**

~~~

Object={
		}
		
And 

Array = [ , , ]

GetElementByTag

= wordt
== equal to
+= was and new

examples:
Array: 
var mygifs[ ];
mygifs=["car","boat","bff"]
if we choose to have "bff" then we write: mygifs(2);

mygifs.shift (adds a value)

We array: 
1. shift
2. push
3. pop

Object:
var myObj{};
value pairs
	={temp:"10",
		highTemp:[, ,],
		onclick:","
	}
	
~~~

**Interconnect NodeJs to MQTT**

On my Rpi nodejs/project folder we ticked the following in the console:

- sudo npm install --save mqtt

We edit the **index.js** and add the followiing code:

~~~
var mqtt  = require('mqtt');
var client  = mqtt.connect('mqtt://127.0.0.1');

client.on('connect', function () {
client.subscribe('#');
client.publish('/', 'Connected to MQTT-Server');
console.log("\nNodeJS Connected to MQTT-Server\n");
});
// send all messages from MQTT to the Websocket with MQTT topic
client.on('message', function(topic, message){
console.log(topic+'='+message);
io.sockets.emit('mqtt',{'topic':String(topic),payload':String(message)});
});

~~~

**Interconnect WebSockets with MQTT**

In **app.js** Add the reverse from socket backt to MQTT inside the SocketIO code: 

~~~javascript

io.sockets.on('connection', function (socket) {

  socket.on('subscribe', function (data) {
    console.log('Subscribing to '+data.topic);
    socket.join(data.topic);
    client.subscribe(data.topic);
  });
  socket.on('control', function (data) {
    console.log('Receiving for MQTT '+ data.topic + data.payload);
	// TODO sanity check .. is it valid topic ... check ifchannel is "mqtt" and pass message to MQTT server
    client.publish(data.topic, data.payload);
  }); 
});

~~~

**Socket connect, subscribe,store data**

1. Socket connect
2. Subscribe to topic
3. onReceipt of MQTT topic messages

~~~javascript 

 <script type="text/javascript">
      var socket = io.connect(window.location.hostname + ":5000");
      socket.on('connect', function() {
        socket.emit('subscribe', {
              topic: '/ESP/#'
          });

          socket.on('mqtt', function(msg) {
              var msgTopic = msg.topic.split("/");
              var topic = msgTopic[3];
              var id = msgTopic[2];
              console.log(msg.topic + ' ' + msg.payload);
              //console.log('#'+topic.concat(id));
              // Added for storage of all incoming MQTT messages in Array
              storeMQData(topic.concat(id), msg.payload);
              // end insert
              $('#' + topic.concat(id)).html(msg.payload);
          });
 //$('input.cb-value').prop("checked", true);
          $('.cb-value').click(function() {
              var mainParent = $(this).parent('.toggle-btn');
              if ($(mainParent).find('input.cb-value').is(':checked')) {
                  $(mainParent).addClass('active');
                  socket.emit('control', {
                      'topic': "/ESP/" + $(this).parents('div').attr('id'),
                      'payload': "on"
                  });
              } else {
                  $(mainParent).removeClass('active');
                  socket.emit('control', {
                      'topic': "/ESP/" + $(this).parents('div').attr('id'),
                      'payload': "off"
                  });
                  //io.sockets.emit('mqtt',"/ESP/unit2/pump=off");
                  //socket.emit('closecmd', id);
              }
          });
      });
  </script>
  
~~~

**Scada**
Controlling Things using Socket.io / nodejs / RaspberryPi gpio

**Map Rpi gpIO Pins Hardware & Software**

![tag](../img/145.png)

**Built the circuit**

![tag](../img/146.jpeg)

**Add scada project to new project folder**

>nodejs
>>projects
>>>project2
>>>>scada

- **npm install express**

![tag](../img/124.png)

- **npm install socket.io**

![tag](../img/125.png)

- **sudo apt-get install pigpio**

![tag](../img/324.png)

- **npm install pigpio**

![tag](../img/325.png)

- **sudo node app.js**

![tag](../img/326.png)

**The code in app.js**

~~~javascript

var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);
var path = require('path');
// Now setup the local hardware IO
var Gpio = require('pigpio').Gpio;
// start your server
var port=4000;
server.listen(port, (err) => {
  if (err) {
    return console.log('something bad happened', err)
  }

  console.log('server is listening on port 4000')
})

// routing your client app (stored in the /public folder)
app.use(express.static(path.join(__dirname, 'public')));


// Handling Socket messages  as soon as socket becomes active
io.sockets.on('connection', function (socket) {
	// Hookup button behaviour within socket to submit an update when pressed (TO DO)

	// when the client emits 'opencmd', this listens and executes
	socket.on('opencmd', function (data) {
		// Add calls to IO here
		io.sockets.emit('opencmd', socket.username, data);
		setTimeout(function () {
			io.sockets.emit('openvalve', socket.username, data);
			console.log("user: "+socket.username+" opens LED" + data);
			// add some error handling here
			led = new Gpio(parseInt(data), {mode: Gpio.OUTPUT});
			led.digitalWrite(1);
		}, 1000)
	});

	// when the client emits 'closecmd', this listens and executes
	socket.on('closecmd', function (data) {
		// Add calls to IO here
		io.sockets.emit('closecmd', socket.username, data);
		setTimeout(function () {
			io.sockets.emit('closevalve', socket.username, data);
			console.log("user: "+socket.username+" closes LED" + data);
			// add error handling here
			led = new Gpio(parseInt(data), {mode: Gpio.OUTPUT});
			led.digitalWrite(0);
		}, 1000)
		
/* 		setTimeout(function () {
			led.digitalWrite(0);
			io.sockets.emit('closevalve', socket.username, data);
		}, 1000) */
	});

});

~~~

**The code in index.html**

~~~

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
<meta http-equiv="X-UA-Compatible" content="IE=9" />

<head>
    <title>Codettes Tutorial</title>
    <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0" />
    <link type="text/css" rel="stylesheet" href="css/app.css" media="all">
</head>
<script src="js/socket.js"></script>
<script src="js/jquery.js"></script>
<script>
    var socket = io.connect(window.location.hostname + ":4000"); //'http://192.168.1.163:4000'); //set this to the ip address of your node.js server

    // listener, whenever the server emits 'openvalve', this updates the username list
    socket.on('opencmd', function(username, data) {
        $('#' + data + ' > div.feedback > div.circle.status > div.circle.command').removeClass('red').addClass('green');
    });
    socket.on('openvalve', function(username, data) {
        $('#' + data + ' > div.feedback > div.circle.status').removeClass('red').addClass('green');
    });


    // listener, whenever the server emits 'openvalve', this updates the username list
     socket.on('closecmd', function(username, data) {
        $('#' + data + ' > div.feedback > div.circle.status > div.circle.command').removeClass('green').addClass('red');
    });
	socket.on('closevalve', function(username, data) {
        $('#' + data + ' > div.feedback > div.circle.status').removeClass('green').addClass('red');
    });

    // on load of page
    $(function() {

        // when the client clicks OPEN
        $('.open').click(function() {
            var id = $(this).parent().attr("id");;
            console.log("user clicked open : " + id);
            socket.emit('opencmd', id);
        });

        // when the client clicks CLOSE
        $('.close').click(function() {
            var id = $(this).parent().attr("id");;
            console.log("user clicked on close : " + id);
            socket.emit('closecmd', id);
        });

    });
</script>

<body>
    <p>

        <div id="17" class="valve">
            <h3>Control 1</h3>
            <div class="open">OPEN</div>
            <div class="close">CLOSE</div>
            <div class="clear"></div>
            <div class="feedback">
                <div class="circle status green">
                    <div class="circle command red"></div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <div id="27" class="valve">
            <h3>Control 2</h3>
            <div class="open">OPEN</div>
            <div class="close">CLOSE</div>
            <div class="clear"></div>
            <div class="feedback">
                <div class="circle status green">
                    <div class="circle command red"></div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <div id="18" class="valve">
            <h3>Control 3</h3>
            <div class="open">OPEN</div>
            <div class="close">CLOSE</div>
            <div class="clear"></div>
            <div class="feedback">
                <div class="circle status green">
                    <div class="circle command red"></div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <div id="23" class="valve">
            <h3>Control 4</h3>
            <div class="open">OPEN</div>
            <div class="close">CLOSE</div>
            <div class="clear"></div>
            <div class="feedback">
                <div class="circle status green">
                    <div class="circle command red"></div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
</body>

~~~

**Now you can turn on and off your Led's**

![tag](../img/129.png)

### Reference

<a href="myFile.js" download>Putty</a>

<a href="myFile.js" download>Winscp</a>

<a href="myFile.js" download>Scada</a>

<a href="myFile.js" download>Chart.js</a> 

