**CAD-CAM**


**CAD-Computer Aided Design**

This is the technology for design and technical documentation, which replaces manual drafting with an automated process.
It is the process of creating product models / designs in 2D or 3D and constraints of both machine & materials.
For the CAD design I used the OpenSCAD software to design parametric.
CAM-Computer Aided Manufacturing
This is the process of creating Machine. For this the Gcode from the CAD  files is needed

**OpenSCAD**

OpenSCAD is software for creating solid 3D CAD objects. 

**The Machine**

- X,Y,Zmax
- Fmin-max
- S(Spindle)(rpm file)
- Controller: Grbl/Lpt/Serial/USB

**Material**

- Size
- Type materials

**Operation**

- Zmax=1/2d
- Smax=20K / Voor hout: Smax=8-10K
- Fmax=3000/ Voor hout: Fmax=1000
- Cut out: inside cut/outside cut/drill/mill

---

### Speakerbox

- Open the OpenSCAD software and click New
- Then put the functions in it for the cube,the pocket and the electronicsport

```
difference(){  
    cube([200,150,9]);
    translate([20,020,0])
        cube([18,100,9]);
    //pocket
     translate([100,75,5])
        cylinder(10,12,12);
    //electronicsport
     translate([200-40,150/2,0])
        cylinder(30,30);
    
```

---

- After putting the function click on preview and render the design. The design will. if it doesn't look good try to play with the values of the sizes.

![tag](../img/scad.PNG)

---

- But this is a 3D design, I can not export it to a 2D design. Now I have to make it a 2D design by putting

```
projection(){
    
```

