**WEEK 3**

**Embedded Programming**

We were working in teams of two people.
The first day I was not able to attend class, so for the documentation of
the first day I refer you to the page of Payne Ellen.

**Assignment**:

- Practicing with Arduino Uno kit using all kindscomponents to make a circuit
- Installing Arduino 1.8.9 for Windows
- Programming basics in the Arduino IDE

**Materials**
- Arduino Uno Board

![tag](../img/1.jpg)

- Breadbord half size

![tag](../img/2.jpeg)

- Circuit connetions shown in diagram,
 including: 1)jumper wires, 2)resistor, 3)LED and 4)a momentary switch

1. ![tag](../img/jumpers.jpg)

2. ![tag](../img/5.jpeg)

3. ![tag](../img/3.jpg)

4. ![tag](../img/4.jpg)


**Learning outcomes**

- Working with The Arduino Uno Board connecting it with our laptops using a USB cable 
and giving the commands by coding it in the Arduino IDE

![tag](../img/computer.PNG)

The serial communications capabilities of the
Arduino platform expose additional capabilities
for your sketches. At a minimum, you can use
serial communication to send data back to the
IDE while you’re troubleshooting your sketches.
To do this, open the IDE’s Tools menu and select
Serial Monitor. A new window opens, and any data
written using the Serial commands will appear in the
monitor window.

- Spaceship Interface

The LED's and the switches are connected to digital pins.

![tag](../img/10.jpeg)

- Procedural programming

**Code the project**

Several things to note are that you can change "switchSate==LOW" to "switchState==HIGH"
in order to switch the behavior of the module. It will blink when unpressed and remain solid when pressed.
The other thing to note is that you can change the value of the "delay" statements to adjust how fast the red LED's
blink when the button is pressed.

**Connect the Arduino to the computer and upload the code**

---

```ruby

int switchState = 0;

void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);   
  pinMode(2, INPUT); 
}

void ledsOff () {
      digitalWrite(3,LOW);
      digitalWrite(4,LOW);
      digitalWrite(5,LOW);
}

void runningLeds() {
      ledsOff();
      digitalWrite(5,HIGH);
      delay(250);
      ledsOff(); 
      digitalWrite(4,HIGH);
      delay(250); 
      ledsOff();
      digitalWrite(3,HIGH);
      delay(250); 

}
// the loop function runs over and over again forever
void loop()  {
   switchState = digitalRead(2);
   
  if (switchState == 0){
      ledsOff();
      digitalWrite(3,HIGH);
  }else{
      runningLeds();
  }

```


--- 

### DAY 2
**Serial Communication**

In the void setup we begin the serial monitor at a rate of 9600.
In the void loop we are using the Serial.println function and what we are printing is the value found in A0 by the Analogread.

---

```

void setup() {
  // setup your serial
  Serial.begin(9600);
  Serial.println('hello world');
  
}

void loop() {
  // Send a message to your serial port/monitor
  Serial.println("change");
  delay(2000);

}

```

---

**LDR**

We used the serial communication to turn off and on a LED.
We created a integer to store the incoming serial byte. 
In the void setup, we set pin 3,4 & 5 as an output and use "Serial.begin(9600)" to initiate the serial communication.
We used 9600 bits per second.
Then we created a integer to run all the leds off and we called it "ledsOff" and to turn the leds on one by one
we created "RunningLeds". We open the serial monitor, which prints different values on the screen.
A potentiometer goes from max and min values of 0 to 1023. Put a black paper on the resistor to prevent light from falling 
on it and that's the minimum value. Put a torch on the resistor and that's the maximum value. We want to read the percent
so we craeted a formula. 
Note* The 100L is important in order to avoid overflowing a 16 bit intergar calculation.

---

``` ruby

//parameter
int minL = 200;
int maxL = 1023;
int lightInt = 0;
int lightPc = 0; 

void setup() {
     pinMode(3,OUTPUT);
     pinMode(4,OUTPUT);
     pinMode(5,OUTPUT);
     
     Serial.begin(9600);
     Serial.println("hallo");
}
// Subroutines
void ledsOff(){
  digitalWrite(3,0);
  digitalWrite(4,0);
  digitalWrite(5,0);
}

void runningLeds(){
  ledsOff();
  digitalWrite(3,1);
  delay(250);
  ledsOff();
  digitalWrite(4,1);
  delay(250);
  ledsOff();
  digitalWrite(5,1);
  delay(250);
}

void loop(){
     lightInt = analogRead(A0);
     //Serial.println(lightInt);
     lightPc = (lightInt-minL)*100L/(maxL-minL);
     Serial.println (lightPc);
     
     
    ledsOff(); 
    if(lightPc>=30){digitalWrite(3,1);};
    if(lightPc>=60){digitalWrite(4,1);};
    if(lightPc>=90){digitalWrite(5,1);};
    if(lightPc> 95){runningLeds();};
    delay(1000);
}

```
--- 

**PWM**

Pulse Width Modulation, or PWM, is a technique for getting analog results with digital means. Digital control is used
to create a square wave, a signal switched between on and off. So if we want to dim a LED, we can change the on and off time of the signal.
If we will change the on and off time fast enough then the brightness of the led will be changed.
And that we're doing with the "dutyCycle". DutyCycle is the percentage of time when the signal was high during the time of period.
So at 10% dutyCycle, the led will be high for 10% and will be low for 90% (100-dutyCycle).
 
Now we want to dim de led with cycleTime. If the cycleTime is lower, the led will go faster on and off.
We set the cylceTime on 50. 

The PWM signal can give the value from 0-255. We created the "potValue" on pin 5.

---

``` ruby
//parameter
int minL = 200;
int maxL = 1023;
int lightInt = 0;
int lightPc = 0; 
int potValue = 0;

void setup() {
     pinMode(3,OUTPUT);
     pinMode(4,OUTPUT);
     pinMode(5,OUTPUT);
     
     Serial.begin(9600);
     Serial.println("hallo");
}
// Subroutines
void ledsOff(){
  digitalWrite(3,0);
  digitalWrite(4,0);
  digitalWrite(5,0);
}

void runningLeds(){
  ledsOff();
  digitalWrite(3,1);
  delay(250);
  ledsOff();
  digitalWrite(4,1);
  delay(250);
  ledsOff();
  digitalWrite(5,1);
  delay(250);
}
void dimled(int ledPin = 3, int dutyCycle = 10){
  int cycleTime = 50;
  digitalWrite (ledPin,1);
  delay (dutyCycle*cycleTime/100);
  digitalWrite (ledPin,0);
  delay ((100 - dutyCycle)*cycleTime/100);
  
}

void loop(){
  potValue = analogRead (A0);
  Serial.println(potValue);
  
 analogWrite(5,potValue/4);
  
//   for (int i =0; i <=255; i++){
//       analogWrite (3, i);
       delay (500);
//}

}

```
---

**DHT11**

The DHT11 is a digital-output relative humidity and temperature sensor. 
- The sensor looks like this

![tag](../img/5.jpeg.png)

My sensor has 3 pins and it's fitted on a board.
Use 3 wires to connect it to the Arduino. The DHT11 senor use just one signal wire to transmit data to the Arduino. The three pin
DHT11 sensor already have a 10 kohm pull up resistor.

![tag](../img/12.jpeg.jpeg)

- You need to add the library to the Arduino.

In the Arduino, go to Sketch >> Include Library >> Add ZIP file.
When you click the 'Add .ZIP library', you should get a file window that pops up. Add the DHT_Library.zip.

![tag](../img/11.jpeg.png)

- Upload the code.

---

```ruby

// Example testing sketch for various DHT humidity/temperature sensors
// Written by ladyada, public domain

// REQUIRES the following Arduino libraries:
// - DHT Sensor Library: https://github.com/adafruit/DHT-sensor-library
// - Adafruit Unified Sensor Lib: https://github.com/adafruit/Adafruit_Sensor

#include "DHT.h"

#define DHTPIN 4    // Digital pin connected to the DHT sensor
// Feather HUZZAH ESP8266 note: use pins 3, 4, 5, 12, 13 or 14
// Pin 15 can work but DHT must be disconnected during program upload.

// Uncomment whatever type you're using!
#define DHTTYPE DHT11   // DHT 11
//#define DHTTYPE DHT22   // DHT 22  (AM2302), AM2321
//#define DHTTYPE DHT21   // DHT 21 (AM2301)

// Connect pin 1 (on the left) of the sensor to +5V
// NOTE: If using a board with 3.3V logic like an Arduino Due connect pin 1
// to 3.3V instead of 5V!
// Connect pin 2 of the sensor to whatever your DHTPIN is
// Connect pin 4 (on the right) of the sensor to GROUND
// Connect a 10K resistor from pin 2 (data) to pin 1 (power) of the sensor

// Initialize DHT sensor.
// Note that older versions of this library took an optional third parameter to
// tweak the timings for faster processors.  This parameter is no longer needed
// as the current DHT reading algorithm adjusts itself to work on faster procs.
DHT dht(DHTPIN, DHTTYPE);

void setup() {
  Serial.begin(9600);
  Serial.println(F("DHTxx test!"));

  dht.begin();
}

void loop() {
  // Wait a few seconds between measurements.
  delay(2000);

  // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  float h = dht.readHumidity();
  // Read temperature as Celsius (the default)
  float t = dht.readTemperature();
  // Read temperature as Fahrenheit (isFahrenheit = true)
  float f = dht.readTemperature(true);

  // Check if any reads failed and exit early (to try again).
  if (isnan(h) || isnan(t) || isnan(f)) {
    Serial.println(F("Failed to read from DHT sensor!"));
    return;
  }

  // Compute heat index in Fahrenheit (the default)
  float hif = dht.computeHeatIndex(f, h);
  // Compute heat index in Celsius (isFahreheit = false)
  float hic = dht.computeHeatIndex(t, h, false);

  Serial.print(F("Humidity: "));
  Serial.print(h);
  Serial.print(F("%  Temperature: "));
  Serial.print(t);
  Serial.print(F("°C "));
  Serial.print(f);
  Serial.print(F("°F  Heat index: "));
  Serial.print(hic);
  Serial.print(F("°C "));
  Serial.print(hif);
  Serial.println(F("°F"));
}

```
---

- When the code is uploaded, open the Serial Monitor. 
- You will see the humidity and temperature.

**Tinkercad**

Tinkercad is a free online collection of software tools that help people all over the world think, create and make. 
We’re the ideal introduction to Autodesk, the leader in 3D design, engineering and entertainment software.

- Make an accout
- After making an account go to circuits and create a new circuit

![tag](../img/13.jpeg.jpeg)

- We learned how to control DC motors using Arduino. 
- First you have to build the circuit

![tag](../img/22.jpeg.PNG)

- We can control the speed of the DC motor by simply controlling the input voltage to the motor and
the most common method of doing that is by using PWM signal.

![tag](../img/17.jpeg.png)

- PWM, or pulse width modulation is a technique which allows us
to adjust the average value of the voltage that’s going to the electronic device by turning on and off the power
at a fast rate.

**Here is the code**
---

```ruby
// Declare ur varables
const int pvm = 3;
const int in_1 = 8;
const int in_2 = 9;


void setup(){
  pinMode(in_1, OUTPUT);
  pinMode(in_2, OUTPUT);
  pinMode(3, OUTPUT);
}

void loop(){
  analogWrite(pvm,64));
    // turn CW
  digitalWrite(in_1, LOW);
  digitalWrite(in_2, HIGH);
  delay(3000);
  }
    //BRAKE
  digitalWrite(in_1, HIGH);
  digitalWrite(in_2, HIGH);
  delay(1000);
  }
   // turn CCW
  digitalWrite(in_1, HIGH);
  digitalWrite(in_2, LOW);
  delay(3000);
  }
    // BRAKE
  digitalWrite(in_1, HIGH);
  digitalWrite(in_2, HIGH);
  delay(1000);
  }

  ```
---

**H-Bridge DC Motor Control**

On the other hand, for controlling the rotation direction,
we just need to inverse the direction of the current flow through the motor, 
and the most common method of doing that is by using an H-Bridge.
So if we combine these two methods, the PWM and the H-Bridge, we can have a complete control over the DC motor.

![tag](../img/20.jpeg.jpeg)

- Jogg button control
(Wiring speed & direction Control. Add a potentiometer and use A0 as the speedPin)

![tag](../img/24.jpeg.png)

**Wiring the speed control**
- Upload the code and open the serial monitor

![tag](../img/21.jpeg.PNG)

**Here is the code**

---

```ruby
// Declare ur varables
const int pvm = 3;
const int in_1 = 8;
const int in_2 = 9;


void setup(){
  pinMode(in_1, OUTPUT);
  pinMode(in_2, OUTPUT);
  pinMode(3, OUTPUT);
  Serial.begin(9600);
}

void loop(){
  int duty = analogRead(A0)-512/2;
  Serial.println(duty);
  analogWrite(pvm,abs(duty));
  if (duty>0){
    // turn CW
  digitalWrite(in_1, LOW);
  digitalWrite(in_2, HIGH);
  }
  if(duty<0){
    // turn CW
  digitalWrite(in_1, HIGH);
  digitalWrite(in_2, LOW);
  }
  if(duty==0){  
    // BRAKE
  digitalWrite(in_1, HIGH);
  digitalWrite(in_2, HIGH);
  }
}

```
---

- Mapping function

The code for the mapping function

---

```ruby
// Declare ur variables
const int pwm = 3;
Const int jogPin =A0; // connects the pot-meter
const int in_1 = 8;
const int in_2 = 9;
Const int deadZone-5; 	//after starts repond
Int duty=0; 		// duty cycle for PWM


void setup(){
	pinMode(in_1, OUTPUT);	
  	pinMode(in_2, OUTPUT);
  	pinMode(3, OUTPUT);
  	Serial.begin(9600);
}

void loop(){
 duty = map(analogRead(jogPin),0,1023,-255,255);
  	Serial.println(duty);
  	analogWrite(pwm,abs(duty)); // abs duty to PWM
  	if(duty> 0 - deadZone){
        // turn CW
      digitalWrite(in_1,LOW);
      digitalWrite(in_2,HIGH); 
    }
  	if(duty < 0 - deadZone){
        // turn CCW
      digitalWrite(in_1,HIGH);
      digitalWrite(in_2,LOW); 
    }
  	if(abs(duty) <= deadZone){
        // BRAKE
      digitalWrite(in_1,HIGH);
      digitalWrite(in_2,HIGH); 
    }  
}
```
---

**Motor control Servo map**

A Servo Motor is a small device that has an output shaft. This shaft can be positioned to specific angular positions by sending the servo a coded signal.
As long as the coded signal exists on the input line, the servo will maintain the angular position of the shaft.
If the coded signal changes, the angular position of the shaft changes.
Build the circuit using a servo motor:

- Attach 5V and ground to one side of the breadboard from the Arduino
- Place a potentiometer on the breadboard. A potentiometer is a type of voltage divider. Turning the knob, change the ratio of the voltage between the middle pin and power.
- Connect one side to the ground and the other to 5V. Connect the middle pin to Analog pin A0. This will control the position of the servo meter.
- The Servo has three pins. One is the power, Second is ground and the third is the control line that will receive information from the Arduino.
- Connect the control line to pin 9 using a wire.

When a Servo motor starts to move, it draws more current than if it were already in motion. This will cause a dip in the voltage on the breadboard.
By placing a 100uF capaciter across power and ground, it smoothes any voltage that occur.

![tag](../img/servo.png)

**Here is the code**
---

```ruby
#include <Servo.h>
Servo myServo;
int const potPin = A0;
int potVal;
int angle;
 
void setup(){
  myServo.attach(9);
  Serial.begin(9600);
}

void loop(){
  potVal = analogRead(potPin);
  Serial.print("potVal: ");
  Serial.print(potVal);
  
  angle = map(potVal,0,1023,0,179);
  Serial.print(",angle: ");
  Serial.println(angle);
    
  myServo.write(angle);
  delay(13);
}
  
```
---

**The servo motor is going to rotate**

![tag](../img/rotate.png)
  
  

