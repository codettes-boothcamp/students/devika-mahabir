# About me

![devika](img/devika.jpg)

---

<BR> Hi, I'm Devika Mahabir and i'm 24. I live in Suriname.
Finished High School. I also like to draw. I never thought i would have ended up here this stage as a woman creating and innovating technology as a part of the IT world.

---

### Final Project

My final project is a Smart Video Doorbell named **Scleraya**


Nowadays, Security system is one of the most researched fields and with increasing security threats, companies are launching new smart security products to combat these threats. IoT is an added advantage in this field which can automatically trigger an event.
A ring at the doorbell may inflame a sense of expectation, suspense, secrecy, hazard or even intrigue.
This Smart doorbell can easily be powered by a 3.7V Battery and whenever someone at the door presses the doorbell button, it will play a specific song on your phone and sends a text message with a link of video streaming page where you can see the person at the door/gate.

----

**The Design**

1 |  2
--- | ---
![tag](img/dev1.jpg) | ![tag](img/dev2.jpg)

---

**Business Model Canvas and Pitchdeck** 

- **Links**

[Business Canvas](https://canvanizer.com/canvas/rNnPfECJRVmkT)

[Pitchdeck Scleraya](https://docs.google.com/presentation/d/1p9hb_un-i3Vats08UJ-YdSd4wF94hnaFIsxfTGB6u1A/edit?usp=sharing)


### Hack o'mation

The HackOmation is a yearly challenge for innovators combining skills in software, electronics and mechanics.

**What is IoT?**

The Internet of Things, or IoT, refers to the billions of physical devices around the world that are now connected to the internet,
all collecting and sharing data. 

The 6th edition of the HackOmation Internet of Things challenge theme :
**Sustainable Tourism for Suriname** and **Health (Covid-19 Related)**

**Team leadHERs**

Project Hack o'mation: **Hydropower plant** (Water turbine)
Product name: **Profusu**

![tag](img/hack1.png)

Electricity can be generated in different ways. One of the ways is through hydropower.
We are building a water turbine (hydropower plant) that can be use by everyone. It will be inexpensive so everybody can buy it.
This will be use to generate electricity for small devices when you are outdoor. This can be very usefull when you are at a place with no electricity.

**General Problem:** No Electricity outdoors.
**For tourists:** Most of the time when tourists go camping or there is no electricity. You start panicking about a low battery and want to charge your phone. And there is no electricity

**Solution:**
Building a water tribune that will generate electricity through the flow of water. It can charge small devices easily. And it is nature friendly.

![tag](img/hack2.jpg)

- Slide

[Pitchdeck Profusu](https://docs.google.com/presentation/d/1KVwCAZpwWlOH4L1Q-ZjSHTBsmRVoA2AgrTwvLbSK_oU/edit?usp=sharing)






